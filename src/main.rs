mod api;
mod commands;
mod structs;
mod utils;

extern crate tokio;

fn main() {
    tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .build()
        .expect("failed to build tokio runtime")
        .block_on(commands::run());
}
