use crate::api;
use crate::structs;
use crate::utils;

use std::env;
use std::process::exit;
use tokio::task::JoinHandle;
extern crate reqwest;
extern crate tokio;

pub async fn run(args: env::Args) {
    let sauces = utils::get_arg_sauces(args);
    let client = api::get_client();
    let mut handles: Vec<JoinHandle<(structs::GalleryInfo, i32)>> =
        Vec::with_capacity(sauces.len());
    for sauce in sauces {
        let cloned_client = client.clone();
        handles.push(tokio::spawn(async move {
            (
                api::get_sauce_info(cloned_client, sauce).await.unwrap(),
                sauce,
            )
        }));
    }
    let mut fail = false;
    let mut one_done = false;
    for handle in handles {
        let (sauce_info, sauce) = handle.await.unwrap();
        match sauce_info {
            structs::GalleryInfo::Info(sauce_info) => {
                if one_done {
                    println!("");
                }
                println!("{}", &sauce_info);
            }
            structs::GalleryInfo::Error(sauce_error) => {
                if one_done {
                    eprintln!("");
                }
                eprintln!("Sauce: {}\nError: {}", sauce, sauce_error.error);
                fail = true;
            }
        };
        one_done = true;
    }
    if fail {
        exit(1);
    }
}
