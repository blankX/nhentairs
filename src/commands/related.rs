use crate::api;
use crate::structs;
use crate::utils;

use std::env;
use std::process::exit;
extern crate reqwest;
extern crate tokio;

pub async fn run(args: env::Args) {
    let sauces = utils::get_arg_sauces(args);
    let is_multi = sauces.len() > 1;

    let client = api::get_client();
    let mut failures = 0;
    let mut one_done = false;

    for sauce in sauces {
        let sauce_info = api::get_related_galleries(&client, sauce).await;
        match sauce_info {
            Ok(structs::RelatedGalleries::Galleries(related_galleries)) => {
                show_related_galleries(sauce, &related_galleries, one_done, is_multi)
            }
            Ok(structs::RelatedGalleries::Error(err)) => {
                show_error(sauce, &err.error, one_done, true);
                failures += 1;
            }
            Err(err) => {
                show_error(sauce, &err, one_done, is_multi);
                failures += 1;
            }
        }
        one_done = true;
    }
    exit(failures);
}

fn show_related_galleries(
    sauce: i32,
    related_galleries: &structs::RelatedGalleriesSuccess,
    prepend_newline: bool,
    is_multi: bool,
) {
    if prepend_newline {
        println!("");
    }

    let mut prefix = "";
    if is_multi {
        println!("{}:", sauce);
        prefix = "- ";
    }
    for i in &related_galleries.result {
        let title = i.title.english.as_deref().or(i.title.japanese.as_deref());
        println!("{}{}: {}", prefix, i.id, title.unwrap_or("<unknown title>"));
    }
}

fn show_error<T: std::fmt::Display>(
    sauce: i32,
    error: &T,
    prepend_newline: bool,
    prepend_sauce: bool,
) {
    if prepend_newline {
        eprintln!("");
    }

    if !prepend_sauce {
        eprintln!("{}", error);
    } else {
        eprintln!("{}: {}", sauce, error);
    }
}
