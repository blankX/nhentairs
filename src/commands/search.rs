use crate::api;

use std::env;
use std::process::exit;
extern crate reqwest;

pub async fn run(args: env::Args) {
    let mut query = String::new();
    for arg in args {
        query.push_str(&format!(" {}", arg));
    }
    let query = query.trim();
    if query.is_empty() {
        eprintln!("Missing search query");
        exit(1);
    }
    let search_info = api::get_search_info(api::get_client(), &query)
        .await
        .unwrap();
    if search_info.len() < 1 {
        eprintln!("No results found");
        exit(1);
    }
    for result in search_info {
        println!("{}: {}", result.id, result.title);
    }
}
