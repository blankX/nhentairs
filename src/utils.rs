use std::env;

use std::process::exit;

pub fn get_arg_sauces(args: env::Args) -> Vec<i32> {
    let mut sauces: Vec<i32> = Vec::new();

    for sauce in args {
        let sauce: i32 = match sauce.parse() {
            Ok(sauce) => sauce,
            Err(_) => {
                eprintln!("{} is not a number/sauce", sauce);
                exit(1);
            }
        };
        if !sauces.contains(&sauce) {
            sauces.push(sauce);
        }
    }

    if sauces.len() < 1 {
        eprintln!("Missing sauce(s)");
        exit(1);
    }
    sauces
}
